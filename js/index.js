$(function() {
	$("[data-toggle='tooltip']").tooltip();
	$("[data-toggle='popover']").popover();
	$('.carousel').carousel({
		interval: 2000
	});
			
	$('#contacto').on('show.bs.modal', function (e){
		console.log('el modal contacto se está mostrando');
		$('[id="contactoBtn"]').removeClass('btn-outline-success');
		$('[id="contactoBtn"]').addClass('btn-warning');
		$('[id="contactoBtn"]').prop('disabled', true);
	});
			
	$('#contacto').on('shown.bs.modal', function (e){
		console.log('el modal contacto se mostró');
	});
			
	$('#contacto').on('hide.bs.modal', function (e){
		console.log('el modal contacto se oculta');
	});
			
	$('#contacto').on('hidden.bs.modal', function (e){
		console.log('el modal contacto se ocultó');
		
		$('[id="contactoBtn"]').removeClass('btn-warning');
		$('[id="contactoBtn"]').addClass('btn-outline-success');				
		$('[id="contactoBtn"]').prop('disabled', false);
	});
			
});